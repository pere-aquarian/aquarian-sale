# Copyright 2021 Pere Martínez <pere@aquarian.tech>

{
    "name": "Sale Order Report Hide Quantities",
    "version": "14.0.1.1.0",
    "category": "Sales Management",
    "author": "Aquarian,"
    "Odoo Community Association (OCA)",
    "website": "https://github.com/OCA/sale-workflow",
    "license": "AGPL-3",
    "depends": ["sale", "account"],
    # "demo": ["demo/sale_order_demo.xml"],
    "data": [
        # "security/ir.model.access.csv",
        # "security/security.xml",
        # "reports/account_invoice_report_view.xml",
        "reports/sale_report_view.xml",
    ],
    "installable": True,
}
